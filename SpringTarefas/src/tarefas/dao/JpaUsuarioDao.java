package tarefas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import tarefas.model.Usuario;

@Repository
public class JpaUsuarioDao implements UsuarioDao {
	
	@PersistenceContext
	EntityManager manager;
	
	@Override
	public void adiciona(Usuario usuario) {
		manager.persist(usuario);
	}

	@Override
	public void alteraSenha(Usuario usuario) {
		Usuario usuarioBusca = manager.find(
				Usuario.class, usuario.getLogin());
		usuarioBusca.setSenha(usuario.getSenha());
		manager.merge(usuarioBusca);
	}

	@Override
	public void remove(Usuario usuario) {
		manager.remove(usuario);
	}

	@Override
	public Usuario buscaPorLogin(String login) {
		return manager.find(Usuario.class, login);
	}

	@Override
	public List<Usuario> lista() {
		return manager.createQuery(
				"select t Usuario t").getResultList();
	}

	@Override
	public boolean existeUsuario(Usuario usuario) {
		Usuario usuarioBusca = manager.find(
				Usuario.class, usuario.getLogin());
		boolean conciliaSenhas = 
				usuario.getSenha().equals(usuarioBusca.getSenha()); 
		if (conciliaSenhas)
			return true;
		return false;
	}

}

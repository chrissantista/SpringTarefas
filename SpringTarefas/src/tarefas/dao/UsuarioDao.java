package tarefas.dao;

import java.util.List;

import tarefas.model.Usuario;

public interface UsuarioDao {
	void adiciona(Usuario usuario);
	void alteraSenha(Usuario usuario);
	void remove(Usuario usuario);
	Usuario buscaPorLogin(String login);
	List<Usuario> lista();
	boolean existeUsuario(Usuario usuario);
}

package tarefas.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import tarefas.dao.TarefaDao;
import tarefas.model.Tarefa;

@Transactional
@Controller
public class TarefasController {
	
	@Autowired
	@Qualifier("jpaTarefaDao")
	private TarefaDao dao;
	
	@RequestMapping("/novaTarefa")
	public String form() {
		return "tarefa/formulario";
	}
	
	@RequestMapping("/adicionaTarefa")
	public String adiciona(@Valid Tarefa tarefa, BindingResult result) {
		if (result.hasErrors()) {
			return "tarefa/formulario";
		}
		
		dao.adiciona(tarefa);
		return "tarefa/adicionada";
	}
	
	@RequestMapping("listaTarefas")
	public String lista(Model model) {
		List<Tarefa> tarefas = dao.lista();
		
		model.addAttribute("tarefas", tarefas);
		return "tarefa/lista";
	}
	
	@RequestMapping("removeTarefa")
	public void remove(Tarefa tarefa, HttpServletResponse response) {
		dao.remove(tarefa);
		response.setStatus(200);
	}
	
	@RequestMapping("mostraTarefa")
	public String mostra(Long id, Model model) {
		Tarefa tarefa = dao.buscaPorId(id);
		model.addAttribute("tarefa", tarefa);
		return "tarefa/mostra";
	}
	
	@RequestMapping("alteraTarefa")
	public String altera(Tarefa tarefa) {
		dao.altera(tarefa);
		return "redirect:listaTarefas";
	}
	
	@RequestMapping("finalizaTarefa")
	public String finaliza(long id, Model model) {
		dao.finaliza(id);
		Tarefa tarefa = dao.buscaPorId(id);
		model.addAttribute("tarefa", tarefa);
		return "tarefa/finalizada";
	}
}

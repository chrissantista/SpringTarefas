package tarefas.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tarefas.model.Usuario;

@Repository
public class JdbcUsuarioDao {
	private Connection connection;
	
	@Autowired
	public JdbcUsuarioDao(DataSource dataSource) {
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean existeUsuario(Usuario usuario) {
		Usuario userDb = null;
		String sql = "SELECT * FROM usuarios WHERE login=?";
		try{
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, usuario.getLogin());
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()) {
				userDb = new Usuario();
				userDb.setLogin(rs.getString("login"));
				userDb.setSenha(rs.getString("senha"));
			}
		} catch (Exception e) {
			throw new RuntimeException();
		}
		if (userDb == null)
			return false;
		if (usuario.getSenha().equals(userDb.getSenha()))
			return true;
		else
			return false;
	}
}

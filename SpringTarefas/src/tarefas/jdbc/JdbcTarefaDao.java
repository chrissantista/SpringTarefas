package tarefas.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tarefas.model.Tarefa;

@Repository
public class JdbcTarefaDao {
	private Connection connection;
	
	@Autowired
	public JdbcTarefaDao(DataSource dataSource) {
		try {
			this.connection = dataSource.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException();
		}
	}
	
	public void adiciona(Tarefa tarefa) {
		String sql = "INSERT INTO tarefas VALUES (null,?,null,null)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql, 
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, tarefa.getDescricao());
			stmt.execute();
			ResultSet rs = stmt.getGeneratedKeys();
			
			if (rs.next())
				tarefa.setId(rs.getLong(1));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void altera(Tarefa tarefa) {
		String sql = "UPDATE tarefas SET descricao=?, "
				+ "finalizado=?, dataFinalizacao=? "
				+ "WHERE id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, tarefa.getDescricao());
			stmt.setBoolean(2, tarefa.isFinalizado());
			Date dataSql = dataCalendarParaSql(tarefa.getDataFinalizacao());
			if (dataSql == null)
				stmt.setNull(3, Types.DATE);
			else
				stmt.setDate(3, dataSql);
			stmt.setLong(4, tarefa.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void remove(Tarefa tarefa) {
		String sql = "DELETE FROM tarefas WHERE id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, tarefa.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Tarefa buscaPorId(Long id) {
		Tarefa tarefa = null;
		String sql = "SELECT * FROM tarefas WHERE id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()) {
				tarefa = new Tarefa();
				tarefa.setId(id);
				tarefa.setDescricao(rs.getString("descricao"));
				tarefa.setFinalizado(rs.getBoolean("finalizado"));
				Date dataSql = rs.getDate("DataFinalizacao");
				Calendar calendar = dataSqlParaCalendar(dataSql);
				tarefa.setDataFinalizacao(calendar);
				
				
			} 

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tarefa;
	}
	
	public List<Tarefa> lista() {
		List<Tarefa> tarefas = new ArrayList<Tarefa>();
		Tarefa tarefa;
		String sql = "SELECT * FROM tarefas";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			Calendar calendar = Calendar.getInstance();
			
			while (rs.next()) {
				tarefa = new Tarefa();
				tarefa.setId(rs.getLong("id"));
				tarefa.setDescricao(rs.getString("descricao"));
				tarefa.setFinalizado(rs.getBoolean("finalizado"));
				Date dataSql = rs.getDate("DataFinalizacao");
				calendar = dataSqlParaCalendar(dataSql);
				tarefa.setDataFinalizacao(calendar);
				tarefas.add(tarefa);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tarefas;
	}
	
	public void finaliza(long id) {
		String sql = "UPDATE tarefas SET finalizado=true, "
				+ "dataFinalizacao=NOW() WHERE id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setLong(1, id);
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	// ======Tarefas privadas===================================
	
	private Calendar dataSqlParaCalendar(Date dataSql) {
		if (dataSql == null)
			return null;
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(dataSql.getTime());
		return calendar;
	}
	
	private Date dataCalendarParaSql(Calendar calendar) {
		if (calendar == null)
			return null;
		Date dataSql = new Date(calendar.getTimeInMillis());
		return dataSql;
	}
}

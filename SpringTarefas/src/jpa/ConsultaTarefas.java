package jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import tarefas.model.Tarefa;

public class ConsultaTarefas {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tarefas");
		EntityManager manager = factory.createEntityManager();
		List<Tarefa> tarefas = manager.createQuery(
				"select t from Tarefa as t where t.finalizado = false").getResultList();
		manager.close();
		for (Tarefa tarefa : tarefas) {
			System.out.println(tarefa.getId() + " - " + tarefa.getDescricao());
		}
	}

}

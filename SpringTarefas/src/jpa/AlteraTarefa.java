package jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import tarefas.model.Tarefa;

public class AlteraTarefa {

	public static void main(String[] args) {
		Tarefa tarefa = new Tarefa();
		tarefa.setId(2);
		tarefa.setDescricao("Teste mudado");
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tarefas");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.merge(tarefa);
		manager.getTransaction().commit();
		manager.close();
	}

}

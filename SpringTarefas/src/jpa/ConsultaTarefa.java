package jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import tarefas.model.Tarefa;

public class ConsultaTarefa {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tarefas");
		EntityManager manager = factory.createEntityManager();
		Tarefa tarefa = manager.find(Tarefa.class, 3L);
		manager.close();
		System.out.println(tarefa.getDescricao());
	}

}

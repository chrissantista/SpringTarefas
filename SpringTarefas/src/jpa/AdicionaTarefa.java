package jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import tarefas.model.Tarefa;

public class AdicionaTarefa {

	public static void main(String[] args) {
		Tarefa tarefa = new Tarefa();
		tarefa.setDescricao("abcdef");
		tarefa.setFinalizado(false);
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tarefas");
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		manager.persist(tarefa);
		manager.getTransaction().commit();
		System.out.println("ID: " + tarefa.getId());
		manager.close();
	}

}

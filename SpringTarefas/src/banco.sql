drop database if exists db_tarefa;
create database db_tarefa;

use db_tarefa;

create table tarefas (
	id BIGINT NOT NULL AUTO_INCREMENT,
	descricao VARCHAR(255),
	finalizado BOOLEAN,
	dataFinalizacao DATE,
	primary key (id)
);

create table usuarios (
	login varchar(255) not null,
	senha varchar(255) not null,
	primary key (login)
);

insert into usuarios values ('chris','1450');

select * from tarefa;
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="chr" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="resources/js/jquery-3.1.1.js"></script>
<script src="resources/js/jquery-ui.js"></script>
<link href="resources/css/jquery-ui.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alterar tarefa - ${tarefa.id}</title>
</head>
<body>

	<h3>Alterar tarefa - ${tarefa.id}</h3>

	<form action="alteraTarefa" method="post">
		<input type="hidden" name="id" value="${tarefa.id}" />
		Descrição: <br>
		<textarea name="descricao" cols="100" rows="5">${tarefa.descricao}</textarea>
		<br>
		Finalizado: <input type="checkbox" name="finalizado" 
			value="true" ${tarefa.finalizado ? 'checked' : ''} /><br>
		Data de finalização: 
		<fmt:formatDate value="${tarefa.dataFinalizacao.time}" pattern="dd/MM/yyyy" var="dataFormatada" />
		<chr:campoData id="dataFinalizacao" value="${dataFormatada}" />
		<br><br>
		<input type="submit" value="Alterar" />
	</form>

</body>
</html>
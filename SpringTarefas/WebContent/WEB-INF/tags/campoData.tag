<%@ attribute name="id" required="true" %>
<%@ attribute name="value" required="false" %>

<input type="text" id="${id}" name="${id}" value="${value}" /><br>
<script>
	$("#${id}").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy'
	});
</script>